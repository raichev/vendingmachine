<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Models\Item;

class ItemTest extends TestCase
{
    private $item;

    public function setUp(): void
    {
        $this->item = new Item('Mars');
    }

    public function testItemName()
    {
        $this->assertEquals('Mars', $this->item->getName());
    }
}
