<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Models\Slot;
use VendingMachine\Models\Item;

class SlotTest extends TestCase
{
    private $slot;
    private $items;
    public function setUp(): void
    {
        foreach (['Snickers', 'Mars', 'Bounty'] as $item) {
            $this->items[] = new Item($item);
        }

        $this->slot = new Slot(1, 1.05, $this->items);
    }

    public function testSlotId()
    {
        $this->assertEquals(1, $this->slot->getId());
    }

    public function testSlotPrice()
    {
        $this->assertEquals(1.05, $this->slot->getPrice());
    }
}
