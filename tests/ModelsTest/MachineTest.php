<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Models\Machine;
use VendingMachine\Models\Slot;
use VendingMachine\Models\Item;
use VendingMachine\Models\Coin;
use VendingMachine\Services\VisualisationService;

class MachineTest extends TestCase
{
    private $machine;

    public function setUp(): void
    {
        $config = [
            'available_coins' => [
                ['count' => 1, 'symbol' => '$2', 'value' => 2.00],
            ],

            'allowed_coins' => [
                ['symbol' => '$2', 'value' => 2.00],
            ],

            'slots' => [
                ['id' => 1, 'price' => 2.00, 'items' => ['Snickers', 'Snickers', 'Snickers']],
            ],
        ];

        $this->machine = new Machine($config);
    }

    public function testGetSlots()
    {
        $expected = [new Slot(1, 2.00, [
            new Item('Snickers'),
            new Item('Snickers'),
            new Item('Snickers'),
        ])];
        $this->assertEquals($expected, $this->machine->getSlots());
    }

    public function testGetAvailableCoins()
    {
        $expected = [new Coin('$2', 2.00)];
        $this->assertEquals($expected, $this->machine->getAvailableCoins());
    }

    public function testGetAllowedCoins()
    {
        $expected = [new Coin('$2', 2.00)];
        $this->assertEquals($expected, $this->machine->getAllowedCoins());
    }

    public function testInsertIcon()
    {
        $visualisation = $this->createMock(VisualisationService::class);
        $visualisation->expects($this->once())
            ->method('tenderedRow');
        $coin = new Coin('$2', 2.00);
        $this->machine->insertCoin($coin, $visualisation);
    }

    public function testExceptionForNeedMoreCoins()
    {
        $visualisation = $this->createMock(VisualisationService::class);
        $slot = $this->createMock(Slot::class);
        $slot->expects($this->once())
             ->method('getPrice')
             ->willReturn(4.00);
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->any())
                ->method('getUserCoins')
                ->willReturn([new Coin('$1', 1.00), new Coin('2', 2.00)]);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('You need to insert more coins');
        $this->machine->vend($slot, $visualisation);
    }

    public function testExceptionForOutOfStock()
    {
        $visualisation = $this->createMock(VisualisationService::class);
        $slot = $this->createMock(Slot::class);
        $slot->expects($this->once())
             ->method('getItems')
             ->willReturn([]);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('This item is out of stock, please choose other');
        $this->machine->vend($slot, $visualisation);
    }

    public function testCheckChangeReturnFalse()
    {
        $visualisation = $this->createMock(VisualisationService::class);
        $visualisation->expects($this->once())
                      ->method('canNotReturnChangeRow');
        $slot = $this->createMock(Slot::class);
        $slot->expects($this->exactly(2))
             ->method('getItems')
             ->willReturn([new Item('Snickers'), new Item('Snickers')]);
        $slot->expects($this->exactly(2))
             ->method('getPrice')
             ->willReturn(2.00);
        $this->machine->insertCoin(new Coin('$3', 3.00), $visualisation);
        $this->machine->vend($slot, $visualisation);
    }

    public function testCheckChangeReturnTrue()
    {
        $slot = new Slot(1, 2.00, [new Item('Snickers'), new Item('Snickers'), new Item('Snickers')]);
        $visualisation = $this->createMock(VisualisationService::class);
        $visualisation->expects($this->once())
            ->method('itemChangeRow');
        $this->machine->insertCoin(new Coin('$4', 4.00), $visualisation);
        $this->machine->vend($slot, $visualisation);
    }
}
