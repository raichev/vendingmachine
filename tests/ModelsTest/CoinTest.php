<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Models\Coin;

class CoinTest extends TestCase
{
    private $coin;

    public function testGetSymbol()
    {
        $coin = new Coin('$1', 1.00);
        $this->assertEquals('$1', $coin->getSymbol());
    }

    public function testGetSymbolOfCents()
    {
        $coin = new Coin('50c', 0.50);
        $this->assertEquals('50c', $coin->getSymbol());
    }

    public function testGetValue()
    {
        $coin = new Coin('$1', 1.00);
        $this->assertEquals(1.00, $coin->getValue());
    }

    public function testGetValueOfCents()
    {
        $coin = new Coin('50c', 0.50);
        $this->assertEquals(0.50, $coin->getValue());
    }

    public function testToString()
    {
        $coins = [
            new Coin('$2', 2.00),
            new Coin('$1', 1.00),
            new Coin('50c', 0.50),
        ];

        $expected = '$2, $1, 50c';
        $this->assertEquals($expected,  implode(', ', $coins));
    }
}
