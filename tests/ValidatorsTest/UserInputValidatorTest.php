<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Models\Coin;
use VendingMachine\Models\Machine;
use VendingMachine\Models\Slot;
use VendingMachine\Services\VisualisationService;
use VendingMachine\Validators\UserInputValidator;

class UserInputValidatorTest extends TestCase
{

    public function testSlotValidator()
    {
        $slot = $this->createMock(Slot::class);
        $slot->expects($this->once())
             ->method('getId')
             ->willReturn('5');
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->once())
                ->method('getSlots')
                ->willReturn([$slot]);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "slot 5";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $userInputVisualisation->validator();
    }

    public function testGetExceptionFromValidator()
    {
        $machine = $this->createMock(Machine::class);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "test1";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid input, please put your coins first and after that choose the slot');
        $userInputVisualisation->validator();
    }

    public function testGetExceptionFromValidateCoin()
    {
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->once())
            ->method('getAllowedCoins')
            ->willReturn([new Coin('50c', 0.50)]);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "30c";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('This coin is not available for this vending machine, please enter again');
        $userInputVisualisation->validator();
    }

    public function testCoinValidatorFirstSymbol()
    {
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->once())
                ->method('getAllowedCoins')
                ->willReturn([new Coin('$2', 2.00)]);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "$2";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $userInputVisualisation->validator();
    }

    public function testCoinValidatorLastSymbol()
    {
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->once())
            ->method('getAllowedCoins')
            ->willReturn([new Coin('50c', 0.50)]);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "50c";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $userInputVisualisation->validator();
    }

    public function testGetWantedException()
    {
        $slot = $this->createMock(Slot::class);
        $slot->expects($this->once())
            ->method('getId')
            ->willReturn('5');
        $machine = $this->createMock(Machine::class);
        $machine->expects($this->once())
            ->method('getSlots')
            ->willReturn([$slot]);
        $visualisation = $this->createMock(VisualisationService::class);
        $input = "slot 10";
        $userInputVisualisation = new UserInputValidator($input, $machine, $visualisation);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Unknown slot number, please enter again');
        $userInputVisualisation->validator();
    }
}
