<?php

    return [
        'available_coins' => [
                ['count' => 2, 'symbol' => '$2', 'value' => 2.00],
                ['count' => 4, 'symbol' => '$1', 'value' => 1.00],
                ['count' => 2, 'symbol' => '50c', 'value' => 0.50],
                ['count' => 3, 'symbol' => '20c', 'value' =>  0.20],
                ['count' => 4, 'symbol' => '10c', 'value' => 0.10],
                ['count' => 1, 'symbol' => '5c', 'value' => 0.05],
        ],

        'allowed_coins' => [
                ['symbol' => '$2', 'value' => 2.00],
                ['symbol' => '$1', 'value' => 1.00],
                ['symbol' => '50c', 'value' => 0.50],
                ['symbol' => '20c', 'value' => 0.20],
                ['symbol' => '10c', 'value' => 0.10],
                ['symbol' => '5c', 'value' => 0.05],
        ],

        'slots' => [
            ['id' => 1, 'price' => 1.05, 'items' => ['Snickers', 'Snickers', 'Snickers']],
            ['id' => 2, 'price' => 1.00, 'items' => []],
            ['id' => 3, 'price' => 1.25, 'items' => ['Mars', 'Mars', 'Mars']],
            ['id' => 4, 'price' => 2.00, 'items' => ['Twix', 'Twix']],
            ['id' => 5, 'price' => 1.80, 'items' => ['Wispa', 'Wispa']],
            ['id' => 6, 'price' => 0.75, 'items' => ['Twirl', 'Twirl']],
            ['id' => 7, 'price' => 1.05, 'items' => ['Yorkie', 'Yorkie', 'Yorkie']],
            ['id' => 8, 'price' => 1.05, 'items' => []],
            ['id' => 9, 'price' => 0.75, 'items' => ['Double Decker', 'Double Decker', 'Double Decker']],
            ['id' => 10, 'price' => 1.80, 'items' => ['Galaxy', 'Galaxy']],
            ['id' => 11, 'price' => 1.80, 'items' => ['Crunchie', 'Crunchie', 'Crunchie']],
            ['id' => 12, 'price' => 1.25, 'items' => ['Picnic', 'Picnic']],
            ['id' => 13, 'price' => 2.00, 'items' => ['Kit Kat', 'Kit Kat']],
            ['id' => 14, 'price' => 1.80, 'items' => ['Lion Bar', 'Lion Bar', 'Lion Bar']],
            ['id' => 15, 'price' => 2.00, 'items' => ['Oreo', 'Oreo']],
            ['id' => 16, 'price' => 2.00, 'items' => ['Toffee Crisp']] ,
            ['id' => 17, 'price' => 1.50, 'items' => ['Boost']],

        ],
    ];