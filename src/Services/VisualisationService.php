<?php

namespace VendingMachine\Services;

use VendingMachine\Models\Slot;
use VendingMachine\Models\Machine;
use VendingMachine\Validators\UserInputValidator;

class VisualisationService
{
    private $machine;

    /**
     * VisualisationService constructor.
     * @param Machine $machine
     */
    public function __construct(Machine $machine)
    {
        $this->machine = $machine;
        $this->startUpScreen();
    }

    /**
     * Render startup screen
     */
    private function startUpScreen() : void
    {
        /** @var $slot Slot */
        foreach ($this->machine->getSlots() as $slot) {
            if (count($slot->getItems()) > 0) {
                echo 'Slot ',$slot->getId(),' - ',count($slot->getItems()),' x ',$slot->getItems()[0]->getName(),' = ',
                number_format($slot->getPrice(), 2),"\n";
            }
        }

        echo "\nThe vending machine accepts the following coins\n";

        echo implode(', ', $this->machine->getAllowedCoins());

        echo "\n\nPlease insert coins one at a time and pressing enter after each, e.g. $2 or 5c\n";

        echo "\nTo vend from a slot type slot command, e.g. slot 1\n\"";

        $this->enterRow();
    }

    /**
     * Render enter row in console
     */
    public function enterRow() : void
    {
        echo "\nEnter: ";
        $inputValue = trim(fgets(STDIN));
        $this->checkInputValue($inputValue);
    }

    /**
     * Render tendered row with count of user coins in console
     */
    public function tenderedRow() : void
    {
        echo "\nTendered = ".number_format($this->machine->countUserCoinValues(), 2)."\n";
    }

    /**
     * Render cannot change row
     */
    public function canNotReturnChangeRow() : void
    {
        echo "We cannot return change\n";
    }

    /**
     * Render final row after every buying with name of item and change
     *
     * @param string $item
     * @param array $change
     */
    public function itemChangeRow(string $item, array $change) : void
    {
        $change = count($change) > 0 ? implode(', ', $change) : 0.00;
        echo "\nEnjoy!\n";
        echo "Item = {$item}\n";
        echo "Change = {$change}\n\n";
    }

    /**
     * Check entered input value from console
     *
     * @param string $inputValue
     */
    private function checkInputValue(string $inputValue) : void
    {
        $validator = new UserInputValidator($inputValue, $this->machine, $this);

        try {
            $validator->validator();
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }

        $this->enterRow();
    }
}
