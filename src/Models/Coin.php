<?php

namespace VendingMachine\Models;

class Coin
{
    private $symbol;
    private $value;

    public function __construct(string $symbol, float $value)
    {
        $this->symbol = $symbol;
        $this->value = $value;
    }

    /**
     * Get value of coin
     *
     * @return float
     */
    public function getValue() : float
    {
        return $this->value;
    }

    /**
     * Get symbol of coin
     *
     * @return string
     */
    public function getSymbol() : string
    {
        return $this->symbol;
    }

    /**
     * Convert object to array of symbols
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getSymbol();
    }
}
