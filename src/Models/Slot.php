<?php

namespace VendingMachine\Models;

/**
 * Class Slot
 * @package VendingMachine\Models
 */
class Slot
{
    private $id;
    private $price;
    private $items;

    /**
     * Slot constructor.
     * @param string $id
     * @param float $price
     * @param array $items
     */
    public function __construct(string $id, float $price, array $items)
    {
        $this->id = $id;
        $this->price = $price;
        $this->items = $items;
    }

    /**
     * Get Id of slot
     *
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * Get Price of slot
     *
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * Get all items name
     *
     * @return array
     */
    public function getItems() : array
    {
        return array_values($this->items);
    }
}
