<?php

namespace VendingMachine\Models;

use Exception;
use VendingMachine\Services\VisualisationService;

class Machine
{
    private $slots;
    private $userCoins;
    private $availableCoins;
    private $allowedCoins;
    private $config;
    private $returningCoins;

    /**
     * Machine constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->userCoins = [];
        $this->setSlots($this->config['slots']);
        $this->setAvailableCoins($this->config['available_coins']);
        $this->setAllowedCoins($this->config['allowed_coins']);
        $this->returningCoins = [];
    }

    /**
     * Set slots to machine using config file
     *
     * @param array $slotsArray
     */
    private function setSlots(array $slotsArray) : void
    {
        $this->slots = [];
        foreach ($slotsArray as $slot) {
            $items = [];
            foreach ($slot['items'] as $item) {
                $items[] = new Item($item);
            }
            $this->slots[$slot['id']] = new Slot($slot['id'], $slot['price'], $items);
        }
    }

    /**
     * Get all machine slots
     *
     * @return array
     */
    public function getSlots() : array
    {
        return $this->slots;
    }

    /**
     * Set available coins, from config file, which stay in machine
     *
     * @param array $coins
     */
    private function setAvailableCoins(array $coins) : void
    {
        $this->availableCoins = [];
        foreach ($coins as $coin) {
            for ($i = 0; $i < $coin['count']; $i++) {
                $this->availableCoins[] = new Coin($coin['symbol'], number_format($coin['value'], 2));
            }
        }
    }

    /**
     * Get all available coin from machine
     *
     * @return array
     */
    public function getAvailableCoins() : array
    {
        return $this->availableCoins;
    }

    /**
     * Set allowed coins, from config file, which stay in machine
     *
     * @param array $coins
     */
    private function setAllowedCoins(array $coins) : void
    {
        $this->allowedCoins = [];
        foreach ($coins as $coin) {
            $this->allowedCoins[] = new Coin($coin['symbol'], number_format($coin['value'], 2));
        }
    }

    /**
     * Get allowed coins for machine
     *
     * @return array
     */
    public function getAllowedCoins() : array
    {
        return $this->allowedCoins;
    }

    /**
     * Get coins inserted by user
     *
     * @return array
     */
    public function getUserCoins() : array
    {
        return $this->userCoins;
    }

    /**
     * return count of coin values  inserted by user
     *
     * @return float
     */
    public function countUserCoinValues() : float
    {
        $countValueCoins = 0.00;
        foreach ($this->getUserCoins() as $coin) {
            $countValueCoins+= $coin->getValue();
        }
        return $countValueCoins;
    }

    /**
     * Insert valid coins to usersCoins after validation
     *
     * @param Coin $coin
     * @param VisualisationService $visualisation
     */
    public function insertCoin(Coin $coin, VisualisationService $visualisation) : void
    {
        $this->userCoins[] = $coin;

        $visualisation->tenderedRow();
    }

    /**
     * After successful validation get user wanted slot and call method for update balance of slot amount
     *
     * @param Slot $slot
     * @param VisualisationService $visualisation
     * @throws Exception if need more coins or item is out of stock
     */
    public function vend(Slot $slot, VisualisationService $visualisation) : void
    {
        if ($slot->getPrice() > $this->countUserCoinValues()) {
            throw new Exception('You need to insert more coins');
        }
        if (count($slot->getItems()) === 0) {
            throw new Exception('This item is out of stock, please choose other');
        }

        $wantedSlot = $slot->getItems()[0]->getName();
        $getChange = number_format($this->countUserCoinValues(), 2) - number_format($slot->getPrice(), 2);
        $checkChange = $this->checkChange((float)number_format($getChange, 2));
        if ($checkChange === false) {
            $visualisation->canNotReturnChangeRow();
        } else {
            $this->updateSlotAmount($slot);
            $visualisation->itemChangeRow($wantedSlot, $this->returningCoins);
        }
        $this->userCoins = [];
        $this->returningCoins = [];
    }

    /**
     * Check if machine can return change after buying or not
     * and update available coins in machine
     *
     * @param float $change
     * @return bool
     */
    private function checkChange(float $change) : bool
    {
        $getAvailableCoins = $this->getAvailableCoins();
        foreach ($getAvailableCoins as $k => $coin) {
            if ($coin->getValue() <= $change) {
                $this->returningCoins[] = $coin->getSymbol();
                $change = (float)sprintf('%.2f', $change - $coin->getValue());
                unset($getAvailableCoins[$k]);
            }
        }

        if ($change === 0.00) {
            $this->availableCoins = array_merge($getAvailableCoins, $this->userCoins);
            return true;
        }

        return false;
    }

    /**
     * Update slot amount after user buy something
     *
     * @param Slot $vendItem
     */
    private function updateSlotAmount(Slot $vendItem) : void
    {
        $items = $this->slots[$vendItem->getId()]->getItems();
        unset($items[0]);
        $this->slots[$vendItem->getId()] = new Slot($vendItem->getId(), $vendItem->getPrice(), $items);
    }
}
