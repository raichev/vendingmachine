<?php

namespace VendingMachine\Models;

class Item
{
    private $name;

    /**
     * Item constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get name of item
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
