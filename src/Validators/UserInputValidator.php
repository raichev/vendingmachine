<?php

namespace VendingMachine\Validators;

use VendingMachine\Models\Coin;
use VendingMachine\Models\Machine;
use VendingMachine\Models\Slot;
use VendingMachine\Services\VisualisationService;
use Exception;

class UserInputValidator
{
    const NUMBER_OF_SLOT = 1;

    private $input;
    private $machine;
    private $visualisation;
    private $insertedCoin;

    /**
     * UserInputValidator constructor.
     * @param string $input
     * @param Machine $machine
     * @param VisualisationService $visualisation
     */
    public function __construct(string $input, Machine $machine, VisualisationService $visualisation)
    {
        $this->input = $input;
        $this->machine = $machine;
        $this->visualisation = $visualisation;
        $this->insertedCoin;
    }

    /**
     * Validate entered input value and if success call vend or InsertCoin
     * if not success return exception
     *
     * @throws Exception if entered invalid input
     */
    public function validator()
    {
        if (strpos(strtolower($this->input), 'slot ') !== false) {
             $wantedSlot = $this->getWantedSlot();
             $this->machine->vend($wantedSlot, $this->visualisation);
        } elseif ($this->getFirstSymbol() === '$' || $this->getLastSymbol() === 'c') {
            $this->validateCoin();
            $this->machine->insertCoin($this->insertedCoin, $this->visualisation);
        } else {
            $exception = 'Invalid input, please put your coins first and after that choose the slot';
            throw new \InvalidArgumentException($exception);
        }
    }

    /**
     * Return wanted row as Object
     *
     * @return Slot|null
     * @throws Exception if slot isn't found
     */
    private function getWantedSlot()
    {
        $explodeInput = explode(' ', $this->input);
        $slotNumber = $explodeInput[self::NUMBER_OF_SLOT];
        $wantedSlot = null;

        /** @var $slot Slot */
        foreach ($this->machine->getSlots() as $slot) {
            if ($slot->getId() === $slotNumber) {
                $wantedSlot = $slot;
            }
        }

        if (is_null($wantedSlot)) {
            throw new Exception('Unknown slot number, please enter again');
        }

        return $wantedSlot;
    }

    /**
     * Validate every coin entered from user
     *
     * @throws Exception if coin isn't found in available coins
     */
    private function validateCoin() : void
    {
        $coinValue = 0;
        $coinSymbol = null;
        /** @var $coin Coin */
        foreach ($this->machine->getAllowedCoins() as $coin) {
            if ($coin->getSymbol() === strtolower($this->input)) {
                $coinValue = $coin->getValue();
                $coinSymbol = $this->input;
            }
        }

        if (is_null($coinSymbol)) {
            throw new Exception('This coin is not available for this vending machine, please enter again');
        }
        $this->insertedCoin = new Coin($coinSymbol, $coinValue);
    }

    /**
     * Get first symbol of console input
     *
     * @return string
     */
    public function getFirstSymbol() : string
    {
        return substr($this->input, 0, 1);
    }

    /**
     * Get last symbol of console input
     *
     * @return string
     */
    public function getLastSymbol() : string
    {
        return substr($this->input, -1);
    }
}
