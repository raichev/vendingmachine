<?php
require  __DIR__ .'/vendor/autoload.php';

use VendingMachine\Services\VisualisationService;
use VendingMachine\Models\Machine;

$configPath = __DIR__.'/config/config.php';

if (!file_exists($configPath)) {
    echo 'Config file is not exists \n.';
    exit;
}
$config = require_once $configPath;

$machine = new Machine($config);
$visualisationService = new VisualisationService($machine);

